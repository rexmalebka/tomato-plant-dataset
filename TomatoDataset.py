from torch.utils.data import Dataset
import torchvision.transforms as transf
import PIL
from skimage import io
import numpy as np
import os
from os import listdir
import random


class TomatoDataset(Dataset):
    """Tomato dataset for segmentation Network
    files format:
    root/
        class_names.txt
        JPEGImages/
            0.jpg
            1.jpg
            ...
        SegmentationClass/
            0.npy
            1.npy
            ...
        SegmentationClassPNG/
            0.png
            1.png
            ...
        SegmentationClassVisualization/
            0.jpg
            1.jpg

    """
    def __init__(self, root, train=False):
        # root directory
        self.root = root
        # Geometry tranformations for augmenting data
        self.geomtransforms = transf.Compose([
            transf.RandomRotation(20, resample=PIL.Image.BILINEAR),
            transf.RandomHorizontalFlip(),
            transf.RandomVerticalFlip(),
            transf.RandomAffine(degrees=(-10, 10), translate=(0, 0.2)),
        ])
        
        # Color tranformations for augmenting the training images
        self.colortransforms = transf.Compose([
            transf.ColorJitter(
                brightness=0.05, contrast=0.06, saturation=0.06, hue=0.05),
        ])
        
        jpegfolder = os.path.join(self.root, "JPEGImages/")
        segmentationfolder = os.path.join(self.root, "SegmentationClass/")        
        
        jpeglist = listdir(jpegfolder)
        segmlist = listdir(segmentationfolder)
        
        jpeglist.sort()
        segmlist.sort()
        
        if train:
            # 80% training - 20% testing
            self.JPEGImages = [
                    PIL.Image.fromarray(
                        io.imread(jpegfolder+img))
                    for img in jpeglist[:int(len(jpeglist)*0.8)]
                    ]
            self.SegmentationClasss = [
                    np.load(segmentationfolder+classes) 
                    for classes in segmlist[:int(len(jpeglist)*0.8)]
                    ]
        else:
            self.JPEGImages = [
                    PIL.Image.fromarray(io.imread(jpegfolder+img)) 
                    for img in jpeglist[int(len(jpeglist)*0.8):]
                    ]
            self.SegmentationClasss = [
                    np.load(segmentationfolder+classes) 
                    for classes in segmlist[int(len(jpeglist)*0.8):]
                    ]
            
        for n, img in enumerate(self.SegmentationClasss):

            # R G B layer as clasification
            # R for tomato, G for foliage, B for background
            
            blank = np.zeros((*img.shape, 3), dtype=np.uint8)
            
            blank[(img == 2)] = np.array([255, 0, 0])
            blank[(img == 1)] = np.array([0, 255, 0])
            blank[(img == 0)] = np.array([0, 0, 255])
            
            img = PIL.Image.fromarray(blank)
            self.SegmentationClasss[n] = img
    
    def __getitem__(self, index):        

        # same seed same transformation in input and labels
        seed = random.randint(0, 2**32)
        random.seed(seed)
        
        img = self.geomtransforms(self.JPEGImages[index])
        img = transf.Normalize(
                (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)
                )(transf.ToTensor()(
                            transf.Resize(
                                (572, 572)
                                )(self.colortransforms(img))))
        random.seed(seed)

        label = transf.Normalize(
                (0.5,), (0.5,)
                )(transf.ToTensor()(
                    self.geomtransforms(transf.Resize((388, 388))(
                        self.SegmentationClasss[index]))))
            
        return (img, label)
    
    def __len__(self):
        return len(self.SegmentationClasss)
