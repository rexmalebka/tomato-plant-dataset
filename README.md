# Tomato Plant segmentation dataset

Tomato Plant Dataset used for training a Tomato plant segmentation Network using U-Net architecture [Tomato plant segmentation using U-Net](https://gitlab.com/rexmalebka/tomato-plant-segmentation-u-net).

The entire dataset has been made scrapping images of tomato plant in internet.
also used `image-net` dataset for cherry tomato [ImageNet dataset](http://image-net.org/synset?wnid=n07734292)

Propose a Dataset class for Pytorch.

## download commands

I used a list and `wget` command for downloading the images, used `-i urls.txt` for specifying a list file, `-nc` for specifying not downloading repeated files and `-t 3` for specifying just retry 3 times.

```
wget -i urls.txt -nc -t 3
```

## selection

I tried to select images of plants of tomato in agricultural scenes, deleting all the images of tomato cooking, also deleted drawing of tomato.

## labels

Two clases, using `labelme` software [labelme github repo](https://github.com/wkentaro/labelme)

- background class
- foliage class
- tomato class

foliage includes all the tomato that are not ripe enough.

## examples

![labeled 1](labels1.png)
![labeled 2](labels2.png)
![labeled 3](labels3.png)

## Folder Structure

This dataset uses the VOC Pascal 2007 format

```
dataset_voc/
	class_names.txt
		JPEGImages/
			0.jpg
			1.jpg
			..
		SegmentationClass/
			0.npy
			1.npy
			..
		SegmentationClassPNG/
			0.png
			1.png
			..
		SegmentationClassVisualization/
			0.jpg
			1.jpg
			..
```


