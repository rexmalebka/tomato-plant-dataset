from TomatoDataset import TomatoDataset
from torch.utils.data import DataLoader

trainset = TomatoDataset(root='tomato-plant-dataset/dataset_voc/', train=False)
testset = TomatoDataset(root='tomato-plant-dataset/dataset_voc/', train=True)

trainloader = DataLoader(trainset, batch_size=7, shuffle=True, num_workers=2)
testloader = DataLoader(testset, batch_size=7, shuffle=True, num_workers=2)
