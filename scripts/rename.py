from os import listdir, rename
from os.path import exists
import json

imgs = listdir('../dataset2/')

imgs.remove('groundtruth')
# enumerar las imágenes la etiquetadas y cambiarles el nombre empezando desde 0

# 133 ->

for n,k in enumerate(imgs):
    
    if not (exists('../dataset2/groundtruth/'+".".join(k.split('.')[:-1])+'.json' )):
        # si no existe un json existente de la imagen
        rename("../dataset2/"+k, '../dataset2/'+str(n+133)+'.'+k.split('.')[-1])

    """

    data = json.load(open('../dataset2/groundtruth/'+gt))
    nombre = data['imagePath']
    data['imagePath']='../'+str(n)+'.'+nombre.split('.')[-1]

    print(f"{nombre} -> {data['imagePath']} {gt}")

    # rename original image
    rename('../dataset2/groundtruth/'+nombre, "../dataset2/groundtruth/"+data['imagePath'])

    # rename json file
    rename('../dataset2/groundtruth/'+gt, '../dataset2/groundtruth/'+str(n)+'.json')

    with open('../dataset2/groundtruth/'+str(n)+'.json','w') as g:
        g.write(json.dumps(data))
    """
