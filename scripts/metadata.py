from os import listdir
import json

grts = [open('../dataset/groundtruth/'+gt) for gt in listdir('../dataset/groundtruth')]

grts = [json.load(gt) for gt in grts]

metadata = set()
for gt in grts:
    for k in gt.keys():
        metadata.add(k)
