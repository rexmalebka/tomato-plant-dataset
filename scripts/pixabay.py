import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


root = "https://pixabay.com/photos/search/tomato%20plant/pagi="
# subsequent pages https://pixabay.com/photos/search/tomato%20plant/?pagi=2

driver = webdriver.Firefox()


for page_number in range(1,2):
    print(f"page number: {page_number} page request: https://pixabay.com/photos/search/tomato%20plant/?pagi={page_number}")

    driver.get(root+str(page_number))

driver.close()

"""






page_req = requests.get(f"https://pixabay.com/photos/search/tomato%20plant/?pagi={page_number}")

    if page_req.status_code == 200:
        page_content = page_req.content
    else:
        continue

    page_parsed = bs(page_content)
    
    media = page_parsed.find('div',{'class':'search_results'}).findAll('img')

    urls = [m.get('src') for m in media]
    
    for url in urls:
        with open('pixabay.txt','a') as pixabay_file:
            pixabay_file.write(url+"\n")
"""
