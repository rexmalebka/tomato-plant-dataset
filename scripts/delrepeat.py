from shutil import copyfile
import os 

with open('../dataset2/sumas.txt') as f:
    sums = f.read()

sums = [k.split(' ') for k in sums.split('\n')][:-1]

iguales = set((k[0] for k in sums))

print('iguales:', len(iguales))

def dev(k):
    if k[0] in iguales:
        iguales.remove(k[0])
        return True
    else:
        return False

unique = list(filter(dev, sums))

for f in unique:
    copyfile('../dataset2/'+f[1],'../dataset3/'+f[1])
    os.remove('../dataset3'+f[1])
